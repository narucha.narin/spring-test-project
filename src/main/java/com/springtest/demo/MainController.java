package com.springtest.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/main")
public class MainController {
	
	@GetMapping(value = "/")
	public String findSellerData()
	{
		return "Hello World Arm 123";
	}
	
	@PostMapping("/api/postTest")
	public String postMappingTest() 
	{
		return "Call By Post Method";
	}
}


